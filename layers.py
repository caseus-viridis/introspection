import math
from functools import partial
import torch
import torch.nn as nn


class LaplaceAttention(nn.Module):
    def __init__(self):
        super(LaplaceAttention, self).__init__()

    def forward(self, query, key, value):
        return torch.softmax(
            -torch.norm(
                query.unsqueeze(dim=-1) - key.unsqueeze(dim=-2), p=1, dim=-1),
            dim=-1) @ value


class ScaledDotProductAttention(nn.Module):
    def __init__(self, dim_key):
        super(ScaledDotProductAttention, self).__init__()
        self.scaler = math.sqrt(dim_key)

    def forward(self, query, key, value):
        return torch.softmax(
            query @ key.transpose(-1, -2) / self.scaler, dim=-1) @ value


class MultiheadAttention(nn.Module):
    "Multihead attention"

    def __init__(self,
                 dim_input,
                 dim_key,
                 dim_value,
                 dim_output,
                 num_heads=4,
                 attention_mechanism=ScaledDotProductAttention,
                 bias=False):
        super(MultiheadAttention, self).__init__()
        self.dim_input = dim_input
        self.dim_output = dim_output
        self.dim_key = dim_key
        self.dim_value = dim_value
        self.num_heads = num_heads

        self.attention = attention_mechanism(dim_key)
        self.input_query = nn.Linear(
            dim_input, dim_key * num_heads, bias=False)
        self.input_key = nn.Linear(dim_input, dim_key * num_heads, bias=False)
        self.input_value = nn.Linear(
            dim_input, dim_value * num_heads, bias=False)
        self.output = nn.Linear(dim_value * num_heads, dim_output, bias=bias)

    def forward(self, query, key, value):
        q = self.input_query(query).view(query.shape[0], query.shape[1],
                                         self.num_heads, self.dim_key)
        k = self.input_key(key).view(key.shape[0], key.shape[1],
                                     self.num_heads, self.dim_key)
        v = self.input_value(value).view(value.shape[0], value.shape[1],
                                         self.num_heads, self.dim_value)
        q = q.permute(2, 0, 1, 3).contiguous().view(-1, query.shape[1],
                                                    self.dim_key)
        k = k.permute(2, 0, 1, 3).contiguous().view(-1, key.shape[1],
                                                    self.dim_key)
        v = v.permute(2, 0, 1, 3).contiguous().view(-1, value.shape[1],
                                                    self.dim_value)
        return self.output(
            self.attention(q, k,
                           v).view(self.num_heads, query.shape[0],
                                   query.shape[1], self.dim_value).permute(
                                       1, 2, 0, 3).contiguous().view(
                                           query.shape[0], query.shape[1], -1))

    def extra_repr(self):
        return f"dim_input={self.dim_input}, dim_output={self.dim_output}, dim_key={self.dim_key}, dim_value={self.dim_value}, num_heads={self.num_heads}"


class LayerNorm(nn.Module):
    "A layernorm module in the OpenAI style (epsilon inside the square root)"

    def __init__(self, n_state, e=1e-5):
        super(LayerNorm, self).__init__()
        self.g = nn.Parameter(torch.ones(n_state))
        self.b = nn.Parameter(torch.zeros(n_state))
        self.e = e

    def forward(self, x):
        m = x.mean(-1, keepdim=True)
        s = (x - m).pow(2).mean(-1, keepdim=True)
        x = (x - m) / torch.sqrt(s + self.e)
        return self.g * x + self.b


class SelfAttentionOverFeature(nn.Module):
    """
    A self-attention over features instead of space
        Input is either a 2D tensor of shape Batch x Feature, or a 3D tensor of shape Batch x Feature x Space
    """

    def __init__(self,
                 in_features,
                 out_features,
                 bias=True,
                 num_heads=4,
                 attention_mechanism=ScaledDotProductAttention):
        super(SelfAttentionOverFeature, self).__init__()
        self.attn = MultiheadAttention(
            dim_input=in_features,
            dim_key=out_features // num_heads,
            dim_value=out_features // num_heads,
            dim_output=out_features,
            num_heads=num_heads,
            attention_mechanism=attention_mechanism,
            bias=bias)

    def forward(self, x):
        # Rather than modifying attention module, here we massage the input and output to achieve attention over features instead of space
        if x.dim()==2:
            # in the case of 2D input of Batch x Feature, make it Batch x 1 x Feature going into attention, pretending there is a 1D space
            in_trans = lambda _x: _x.unsqueeze(1)
            out_trans = lambda _x: _x.squeeze(1)
        elif x.dim()==3:
            # in the case of 3D input of Batch x Feature x Space, swap dim making it Batch x Space x Feature going in
            in_trans = lambda _x: _x.transpose(1, -1)
            out_trans = in_trans
        else:
            raise RuntimeError("invalid input shape")
        return out_trans(self.attn(
            in_trans(x),
            in_trans(x),
            in_trans(x)
        ))


# A drop-in replacement of nn.Linear using self-attention
Introspection = lambda num_heads=1, attention_mechanism=ScaledDotProductAttention: partial(SelfAttentionOverFeature, num_heads=num_heads, attention_mechanism=attention_mechanism)