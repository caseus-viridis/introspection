import argparse
import os
from glob import glob
from dotenv import load_dotenv
import torch
import torch.nn as nn
from torch.optim import SGD, Adam
from torch.optim.lr_scheduler import MultiStepLR
from pytorch_monitor import init_experiment, monitor_module
from ignite.engine import Engine, Events, create_supervised_trainer, create_supervised_evaluator
from ignite.utils import convert_tensor
from ignite.metrics import Accuracy, Loss, RunningAverage
from ignite.handlers import ModelCheckpoint, EarlyStopping
from ignite.contrib.handlers import ProgressBar, LRScheduler
from data import MNIST
from layers import Introspection
from models import Model

parser = argparse.ArgumentParser(
    description='Training MNIST MLP vs Introspection')
parser.add_argument(
    '-x',
    '--experiment',
    type=str,
    default='introspection',
    help='name of experiment (default: introspection)')
parser.add_argument(
    '-l',
    '--layer',
    type=str,
    default='linear',
    help='type of layer, linear or introspection (default: linear)')
parser.add_argument(
    '-w',
    '--widths',
    type=str,
    default='300,100',
    help='widths of hidden layers (default: 300,100)')
parser.add_argument(
    '-H',
    '--num-heads',
    type=int,
    default=4,
    help='number of attention heads (default: 4)')
parser.add_argument(
    '-ln',
    '--layernorm',
    action='store_true',
    help='layernorm or not (default: False)')
parser.add_argument(
    '-do',
    '--dropout',
    type=float,
    default=0.,
    help='dropout fraction (default: 0.)')
parser.add_argument(
    '-b',
    '--batch-size',
    type=int,
    default=64,
    help='batch size (default: 64)')
parser.add_argument(
    '-e',
    '--epochs',
    type=int,
    default=100,
    help='number of epochs (default: 100)')
parser.add_argument(
    '-j',
    '--num-workers',
    default=0,
    type=int,
    help="num of data loader worker processes (default: 0)",
)
parser.add_argument(
    '--gpu', default='0', type=str, help='id(s) for GPU(s) to use')
parser.add_argument(
    '-m',
    '--monitor',
    action='store_true',
    help='monitoring or not (default: False)')
parser.add_argument(
    '-r', '--run-id', type=int, default=0, help='Run ID (default: 0)')
args = parser.parse_args()

# experiment and run name
exp_name = args.experiment
run_name = f"{args.layer}{'-H{}'.format(args.num_heads) if args.layer=='introspection' else ''}-w{args.widths}{'-ln' if args.layernorm else ''}-do{args.dropout}-b{args.batch_size}-sgd-run{args.run_id}"

# gpu
os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu
device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
torch.backends.cudnn.benchmark = True

# env
load_dotenv(verbose=True)
DATA_PATH = os.getenv("DATA_PATH") or './data'
MONITOR_PATH = os.getenv("MONITOR_PATH") or './monitor'
CHECKPOINT_PATH = os.getenv("CHECKPOINT_PATH") or './checkpoint'

# monitor
if args.monitor:
    writer, config = init_experiment({
        'title':
        "Introspection experiments",
        'run_name':
        run_name,
        'log_dir':
        '/'.join([MONITOR_PATH, exp_name]),
        'random_seed':
        args.run_id
    })

# checkpointer
checkpointer = ModelCheckpoint(
    dirname='/'.join([CHECKPOINT_PATH, exp_name]),
    filename_prefix=run_name,
    save_interval=1,
    require_empty=False,
    save_as_state_dict=True)


# define checkpoint structure
class assemble_checkpoint:
    def state_dict(self):
        return dict(
            model_state_dict=model.state_dict(),
            optimizer_state_dict=optimizer.state_dict(),
            trainer_state=dict(
                epoch=trainer.state.epoch, iteration=trainer.state.iteration),
            checkpointer_state=dict(_iteration=checkpointer._iteration, ))


def get_layer(cfg):
    if cfg.layer=='linear':
        return nn.Linear
    elif cfg.layer=='introspection':
        return Introspection(num_heads=cfg.num_heads)
    else:
        raise RuntimeError("type of layer not supported")

param_count = lambda m: sum(p.numel() for p in m.parameters() if p.requires_grad) if isinstance(m, torch.nn.Module) else 0

data = MNIST(
    data_dir='/'.join([DATA_PATH, 'mnist']),
    cuda=torch.cuda.is_available(),
    num_workers=args.num_workers,
    batch_size=args.batch_size
)

model = Model(
    widths=[w for w in map(int, args.widths.split(','))],
    layer=get_layer(args),
    dropout=args.dropout,
    layernorm=args.layernorm
)
loss_func = nn.CrossEntropyLoss()
# optimizer = Adam(model.parameters(), lr=1.2e-3)
optimizer = SGD(model.parameters(), lr=1e-1, momentum=0.9, nesterov=True)
lr_scheduler = MultiStepLR(optimizer, milestones=[25, 50, 75], gamma=0.2)

# print the model description
print(model)
print("Parameter count = {}".format(param_count(model)))
print("Run name: {}".format(run_name))


# engines
trainer = create_supervised_trainer(model, optimizer, loss_func, device=device)
evaluator = create_supervised_evaluator(
    model,
    metrics={
        'accuracy': Accuracy(),
        'loss': Loss(loss_func)
    },
    device=device)
RunningAverage(alpha=0.9, output_transform=lambda x: x).attach(trainer, 'loss')
ProgressBar().attach(trainer, ['loss'])


# resume from checkpoint
@trainer.on(Events.STARTED)
def resume_from_checkpoint(engine):
    ckpt = glob('/'.join([CHECKPOINT_PATH, exp_name, run_name]) + '*.pth')
    if ckpt:
        print("Resuming from checkpoint: {}".format(*ckpt))
        _ckpt = torch.load(*ckpt)
        model.load_state_dict(_ckpt['model_state_dict'])
        optimizer.load_state_dict(_ckpt['optimizer_state_dict'])
        for k, v in _ckpt['trainer_state'].items():
            setattr(trainer.state, k, v)
        for k, v in _ckpt['checkpointer_state'].items():
            setattr(checkpointer, k, v)
        checkpointer._saved = [
            (checkpointer._iteration, ckpt)
        ]  # manually mark the checkpoint loaded from as saved
    else:
        if args.monitor:
            writer.add_scalar('param_count', param_count(model), 0)


# log training results
@trainer.on(Events.EPOCH_COMPLETED)
def log_training_loss(engine):
    if args.monitor:
        writer.add_scalar('train_loss', engine.state.metrics['loss'],
                          engine.state.epoch)


# LR scheduler
@trainer.on(Events.EPOCH_STARTED)
def adjust_lr(engine):
    lr_scheduler.step(engine.state.epoch - 1)


# do test and log results
@trainer.on(Events.EPOCH_COMPLETED)
def log_test_results(engine):
    evaluator.run(data.test)
    loss, accuracy = evaluator.state.metrics['loss'], evaluator.state.metrics['accuracy']
    if args.monitor:
        writer.add_scalar('test_loss', loss, engine.state.epoch)
        writer.add_scalar('accuracy', accuracy, engine.state.epoch)


# log other stuff
@trainer.on(Events.EPOCH_COMPLETED)
def log_misc(engine):
    if args.monitor:
        writer.add_scalar('lr', optimizer.param_groups[0]['lr'], engine.state.epoch)


# print summary to console after each epoch
@trainer.on(Events.EPOCH_COMPLETED)
def print_results(engine):
    print(
        "Epoch {:3d}: train loss = {:.4f}, test loss = {:.4f}, test accuracy = {:.4f}"
        .format(trainer.state.epoch, trainer.state.metrics['loss'],
                evaluator.state.metrics['loss'],
                evaluator.state.metrics['accuracy']))


# save checkpoint
trainer.add_event_handler(Events.EPOCH_COMPLETED, checkpointer,
                          {'ckpt': assemble_checkpoint()})

if __name__ == "__main__":
    trainer.run(data.train, max_epochs=args.epochs)
