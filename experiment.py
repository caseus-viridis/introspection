import os
import time
import argparse
import itertools
import random

parser = argparse.ArgumentParser()
parser.add_argument(
    '--csub',
    action='store_true',
    help='run with csub (default: False)')
parser.add_argument(
    '--spot',
    action='store_true',
    help='run with spot instance (default: False)')
parser.add_argument(
    '--dryrun',
    action='store_true',
    help='dry run (default: False)')
parser.add_argument(
    '-n',
    '--n-runs-each',
    type=int,
    default=1,
    help='number of runs for each config (default: 1)')
parser.add_argument(
    '-i',
    '--submission-interval',
    type=float,
    default=0.1,
    help='interval between job submissions in minutes (default: 0.1)')
args = parser.parse_args()


def get_csub_command(run_name, command):
    return f"csub -d -j4 -t gpu1,{'spot' if args.spot else 'ondemand'} -N {run_name} {command}"


# experiment name
exp_name = 'introspection_mnist'
n_runs_each = args.n_runs_each

# constants
_batch_size = 100

# variables
widths = [(300, 100), (75, 25)]
num_heads = [1, 2, 4]
dropout = [0.0, 0.5]
layernorm = [True]

linear_runs = (
    f"python train.py -x {exp_name} -w {','.join(map(str, _widths))} -l linear {'-ln' if _layernorm else ''} -do {_dropout} -b{_batch_size} -r{run_id} -m"
    for (_widths, _dropout, _layernorm) in itertools.product(
        widths, dropout, layernorm)
    for run_id in range(args.n_runs_each)
)
introspection_runs = (
    f"python train.py -x {exp_name} -w {','.join(map(str, _widths))} -l introspection -H{_num_heads} {'-ln' if _layernorm else ''} -do {_dropout} -b{_batch_size} -r{run_id} -m"
    for (_widths, _num_heads, _dropout, _layernorm) in itertools.product(
        widths, num_heads, dropout, layernorm)
    for run_id in range(args.n_runs_each)
)

runs = itertools.chain(linear_runs, introspection_runs)

for ix, command in enumerate(runs):
    run_name = f"{exp_name}_run{ix}"
    command = get_csub_command(run_name, command) if args.csub else command
    if args.dryrun:
        print(command)
    else:
        os.system(command)
        time.sleep(args.submission_interval*60)

print(f"Total: {ix+1} jobs")
