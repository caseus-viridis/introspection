import torch
import torch.nn as nn
from layers import LaplaceAttention, ScaledDotProductAttention, LayerNorm


class Model(nn.Module):
    def __init__(self, 
            widths=[300, 100], 
            layer=nn.Linear,
            dropout=0., 
            layernorm=False):
        super(Model, self).__init__()
        layers = []
        _width = 784
        for width in widths:
            layers.append(
                layer(
                    in_features=_width,
                    out_features=width,
                    bias=True
                )
            )
            _width = width
            if layernorm:
                layers.append(LayerNorm(_width))
            if dropout>0.: 
                layers.append(nn.Dropout(dropout))    
            layers.append(nn.ReLU(inplace=True))
        layers.append(
            layer(
                in_features=_width,
                out_features=10,
                bias=True
            )
        )
        self.net = nn.Sequential(*layers)
    
    def forward(self, x):
        return self.net(x.view(-1, 784))