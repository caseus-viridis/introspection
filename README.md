# Introspection

This is a prototype of ***Introspection***, a special-case self-attention over feature module.  

## Overview

In a regular ***self-attention module***, input (i.e. query, key, value) is a 3D tensor of shape `Batch x Feature x Space`.  
We say attention is ***over space*** in the sense that a similarity reduction (e.g. dot-product) is over the feature dimension, serving as weights that combine over the space dimension.  

The ***self-attention-over-feature module*** is basically a self-attention over the feature dimension rather than over space.  
Similarity reduction is over space, and then used to combine over features.  

This gives rise to a special-case self-attention-over-feature when the input is 2D (i.e. `Batch x Feature`), instead of 3D/4D like sequences/images (i.e. `Batch x Feature x Space`).  
In this case one can pretend there is 1D space (i.e. `Batch x Feature x 1`) and apply self-attention-over-feature.  
We call this an ***introspection module***, a differentiable transformation from an input feature space to an output feature space.  
Thus, the introspection has input-output interface just like a fully-connected layer.  

Here we implement an `Introspection` module that can be used as a drop-in replacement of a `nn.Linear` layer.  
API:
```python
linear_layer = nn.Linear(in_features=32, out_features=16, bias=True)
```
```python
from layers import Introspection
introspection_layer = Introspection(num_heads=4)(in_features=32, out_features=16, bias=True)
```
Note the additional architectural specification of number of attention heads. 


## Prototype MNIST-MLI vs. MNIST-MLP

Since we make introspection a drop-in replacement of the linear layer, we can turn an MLP into an equivalent MLI.  

This repo contains a prototype `LeNet-300-100` and a `LeNet-75-25` on MNIST.  
The following are model parameter counts and training results.  
Run `experiment.py` to reproduce.  

Parameter counts:

| Architecture | `LeNet-300-100` | `LeNet-75-25` |
|:--|--:|--:|
| MLP | 267.4K | 61.2K |
| MLI(1 head) | 899.9K | 189.4K | 
| MLI(2 head) | 899.9K | 186.8K | 
| MLI(4 head) | 899.3K | 181.7K | 

Test accuracy% (with layer-norm, no drop-out):

| Architecture | `LeNet-300-100` | `LeNet-75-25` |
|:--|--:|--:|
| MLP | 98.59 | 98.00 |
| MLI(1 head) | 98.38 | 97.77 | 
| MLI(2 head) | 98.38 | 97.88 | 
| MLI(4 head) | 98.41 | 97.76 | 

Test accuracy% (with layer-norm, `p=0.5` drop-out):

| Architecture | `LeNet-300-100` | `LeNet-75-25` |
|:--|--:|--:|
| MLP | 98.47 | 96.72 |
| MLI(1 head) | 98.11 | 95.38 | 
| MLI(2 head) | 98.11 | 95.23 | 
| MLI(4 head) | 98.13 | 95.18 | 
